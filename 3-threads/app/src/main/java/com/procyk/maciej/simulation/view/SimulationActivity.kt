package com.procyk.maciej.simulation.view

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.procyk.maciej.simulation.R
import com.procyk.maciej.simulation.utils.text
import com.procyk.maciej.simulation.utils.snack
import com.procyk.maciej.simulation.viewmodel.SimulationViewModel
import com.procyk.maciej.simulation.viewmodel.SimulationViewModel.Companion.validPercString
import com.procyk.maciej.simulation.viewmodel.SimulationViewModel.Companion.validUIntString
import kotlinx.android.synthetic.main.activity_simulation.*

class SimulationActivity : AppCompatActivity() {

    private val model by viewModels<SimulationViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simulation)

        model.paused.observe(this, Observer { paused ->
            buttonPauseResume.text = getString(if (paused) R.string.resume else R.string.pause)
            buttonPauseResume.setOnClickListener {
                it.snack(run {
                    if (paused) {
                        if (model.canResume) {
                            model.resumeSimulation()
                            "Resuming..."
                        }
                        else "Simulation not started"
                    }
                    else {
                        if (model.canPause) {
                            model.pauseSimulation()
                            "Pausing.."
                        }
                        else "Simulation not started"
                    }
                })
            }
        })

        model.started.observe(this, Observer { started ->
            buttonStartStop.text = getString(if (started) R.string.stop else R.string.start)
            buttonStartStop.setOnClickListener {
                it.snack(run {
                    if (started) {
                        if (model.canStop) {
                            model.stopSimulation()
                            "Stopping..."
                        } else  "Simulation not started"
                    }
                    else {
                        val threads = editTextThreads.text()
                        val iter = editTextIterations.text()
                        val deathProb = editTextDeathProbability.text()
                        val infProb = editTextInfectionProbability.text()
                        val infected = editTextStartInfected.text()
                        val sleep = editTextSleepTime.text()

                        val percents = arrayOf(deathProb, infProb, infected)
                        val numbers = arrayOf(threads, iter, sleep, *percents)

                        if (percents.all(::validPercString) && numbers.all(::validUIntString)) {
                            if (model.canStart) {
                                model.runSimulation(
                                    threads = threads.toInt(),
                                    iterations = iter.toInt(),
                                    infectionProbability = infProb.toInt(),
                                    deathProbability = deathProb.toInt(),
                                    infectedPercentage = infected.toInt(),
                                    sleepTime = sleep.toInt()
                                )
                                "Starting..."
                            } else "Simulation in progress"
                        } else "Not valid parameter given"
                    }
                })
            }
        })

        listOf(model.healthy, model.infected, model.dead)
            .zip(listOf(textViewHealthy, textViewInfected, textViewDead))
            .forEach { (model, view) ->
            model.observe(this, Observer {
                view.text = it
            })
        }
        model.healthy.observe(this, Observer {
            textViewHealthy.text = it
        })
    }

}

