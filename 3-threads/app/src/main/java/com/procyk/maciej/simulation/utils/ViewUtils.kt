package com.procyk.maciej.simulation.utils

import android.view.View
import android.widget.EditText
import com.google.android.material.snackbar.Snackbar

fun EditText?.text() = this?.text?.toString() ?: ""

fun View.snack(text: String) = Snackbar.make(this, text, Snackbar.LENGTH_SHORT).show()

