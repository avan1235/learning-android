package com.procyk.maciej.simulation.viewmodel

import androidx.lifecycle.*
import com.procyk.maciej.simulation.model.Life
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.Semaphore
import kotlinx.coroutines.sync.withLock

class SimulationViewModel() : ViewModel() {

    private val _healthy = MutableLiveData<String>().apply { value = formatHealthy(0) }
    val healthy: LiveData<String> = _healthy

    private val _infected = MutableLiveData<String>().apply { value = formatInfected(0) }
    val infected: LiveData<String> = _infected

    private val _dead = MutableLiveData<String>().apply { value = formatDead(0) }
    val dead: LiveData<String> = _dead

    private fun formatHealthy(number: Int) = String.format("Healthy: %d", number)
    private fun formatInfected(number: Int) = String.format("Infected: %d", number)
    private fun formatDead(number: Int) = String.format("Dead: %d", number)

    private val _paused = MutableLiveData<Boolean>().apply { value = false }
    val paused: LiveData<Boolean> = _paused

    private val _started = MutableLiveData<Boolean>().apply { value = false }
    val started: LiveData<Boolean> = _started

    private var simulationJob: Job? = null

    fun pauseSimulation() {
        pauseSemaphore = Semaphore(threads, threads)
        _paused.value = true
    }

    fun resumeSimulation() {
        _paused.value = false
        repeat(threads) {
            pauseSemaphore?.release()
        }
    }

    fun stopSimulation() {
        resumeSimulation()

        viewModelScope.launch {
            simulationJob?.cancelAndJoin()
            simulationJob = null

            mutex.withLock {
                healthyCount = 0
                infectedCount = 0
                deadCount = 0
            }
            publishProgress()
            _started.value = false
        }
    }

    val canStart: Boolean
        get() { return !_started.value!! }

    val canStop: Boolean
        get() { return _started.value!! }

    val canPause: Boolean
        get() { return !_paused.value!! && _started.value!! }

    val canResume: Boolean
        get() { return _paused.value!! && _started.value!! }

    /*
     * To emulate background tasks the new Kotlin suspending
     * functions are used which are not only the lighter version
     * of threads but also allows to easily modify Adroid UI inside
     * them while running in parallel
     */

    @Volatile private var infectedCount = 0
    @Volatile private var healthyCount = 0
    @Volatile private var deadCount = 0
    private val mutex = Mutex()

    private var threads = 0
    private var pauseSemaphore: Semaphore? = null

    fun runSimulation(threads: Int, iterations: Int,
                      infectionProbability: Int, deathProbability: Int,
                      infectedPercentage: Int, sleepTime: Int) {
        val infected = infectedPercentage * threads / 100
        val notInfected = threads - infected
        _started.value = true
        this.threads = threads

        simulationJob = viewModelScope.launch {
            mutex.withLock {
                healthyCount = notInfected
                infectedCount = infected
                deadCount = 0
            }

            repeat(infected) {
                async(context = coroutineContext) { singleLife(infectionProbability, deathProbability, sleepTime, iterations, isInfected = true) }
            }
            repeat(notInfected) {
                async(context = coroutineContext) { singleLife(infectionProbability, deathProbability, sleepTime, iterations, isInfected = false) }
            }
        }.apply {
            invokeOnCompletion { _started.value = false }
        }
    }

    private suspend fun singleLife(infProb: Int, deathProb: Int, sleepTime: Int, iterations: Int, isInfected: Boolean) =
        withContext(Dispatchers.IO) {
            val life = Life(infProb, deathProb, isInfected)
            var i = 0
            while (isActive && life.isAlive && i < iterations) {
                while (_paused.value!!) {
                    pauseSemaphore?.acquire()
                }

                delay(sleepTime.toLong())

                val currentInfected = infectedCount
                if (currentInfected <= 0) {
                    break
                }
                val lastState = life.currentState
                life.nextState(currentInfected)
                val newState = life.currentState
                when {
                    lastState == Life.State.HEALTHY && newState == Life.State.INFECTED -> mutex.withLock {
                        healthyCount -= 1
                        infectedCount += 1
                    }
                    lastState == Life.State.INFECTED && newState == Life.State.DEAD -> mutex.withLock {
                        infectedCount -= 1
                        deadCount += 1
                    }
                    lastState == Life.State.INFECTED && newState == Life.State.HEALTHY -> mutex.withLock {
                        infectedCount -= 1
                        healthyCount += 1
                    }
                }
                publishProgress()
                i += 1
            }
        }

    private suspend fun publishProgress() = withContext(Dispatchers.Main) {
        mutex.withLock {
            with(this@SimulationViewModel) {
                _healthy.value = formatHealthy(healthyCount)
                _dead.value = formatDead(deadCount)
                _infected.value = formatInfected(infectedCount)
            }
        }
    }

    companion object {
        fun validPercString(string: String) = (string.toIntOrNull() ?: -1) in (0..100)

        fun validUIntString(string: String) = string.toIntOrNull()?.run { this >= 0 } ?: false
    }
}