package com.procyk.maciej.simulation.model

import kotlin.random.Random.Default.nextBoolean

class Life(val infectionProbabilityPercentage: Int,
           val deathProbabilityPercentage: Int,
           isInfected: Boolean) {

    enum class State {
        HEALTHY,
        INFECTED,
        DEAD
    }

    private var state: State

    init {
        state = if (isInfected) State.INFECTED else State.HEALTHY
    }

    val isAlive: Boolean
        get() {
            return state != State.DEAD
        }

    val currentState: State
        get() {
            return state
        }

    fun nextState(currentInfectedCount: Int) {
        this.state = when(state) {
            State.DEAD -> State.DEAD
            State.INFECTED -> if (matchProbability(deathProbabilityPercentage)) State.DEAD else State.HEALTHY
            State.HEALTHY -> {
                val contacted = (0..currentInfectedCount).random()
                var getInfected = false
                repeat(contacted) { getInfected = getInfected || matchProbability(infectionProbabilityPercentage) }
                if (getInfected) State.INFECTED else State.HEALTHY
            }
        }
    }
}

/**
 * For the specified range of success probability gets
 * random state with the true probable as success/100
 * nd false probable as 1 - success/100
 */
private fun matchProbability(success: Int): Boolean
        = (0..100).random() in (0..success)