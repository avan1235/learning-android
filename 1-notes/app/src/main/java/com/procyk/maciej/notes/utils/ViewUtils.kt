package com.procyk.maciej.notes.utils

import android.widget.EditText

fun EditText?.extractText() = this?.text?.toString() ?: ""
