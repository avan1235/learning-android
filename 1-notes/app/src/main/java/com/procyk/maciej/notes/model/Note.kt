package com.procyk.maciej.notes.model

import java.io.BufferedWriter
import java.io.Serializable

data class Note(
    val title: String = "",
    val description: String = "",
    val body: String = "") : Serializable {

    fun isEmpty(): Boolean = title == "" && description == "" && body == ""

    fun writeToFile(bufferedWriter: BufferedWriter) {
        bufferedWriter.write(
            "$title\n" +
                "$description\n" +
                body)
        bufferedWriter.flush()
    }
}
