package com.procyk.maciej.notes.view

import android.os.Bundle
import android.os.PersistableBundle
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.procyk.maciej.notes.R
import com.procyk.maciej.notes.model.Note
import com.procyk.maciej.notes.model.NoteCollection
import com.procyk.maciej.notes.utils.extractText
import com.procyk.maciej.notes.viewmodel.NoteViewModel
import kotlinx.android.synthetic.main.activity_note.*

class NoteActivity : AppCompatActivity() {

    private val model: NoteViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note)

        model.noteTitle.observe(this, Observer { value ->
            editTextTitle.setText(value, TextView.BufferType.EDITABLE)
            editTextTitle.setSelection(value.length)
        })

        model.noteDescription.observe(this, Observer { value ->
            editTextDescription.setText(value, TextView.BufferType.EDITABLE)
            editTextDescription.setSelection(value.length)
        })

        model.noteBody.observe(this, Observer { value ->
            editTextContent.setText(value, TextView.BufferType.EDITABLE)
            editTextContent.setSelection(value.length)
        })

        nextButton.setOnClickListener {
            model.saveAndLoadNextNote(
                title = editTextTitle.extractText(),
                description = editTextDescription.extractText(),
                body = editTextContent.extractText()
            ) }

        prevButton.setOnClickListener {
            model.saveAndLoadPrevNote(
                title = editTextTitle.extractText(),
                description = editTextDescription.extractText(),
                body = editTextContent.extractText()
            ) }
    }

    override fun onPause() {
        super.onPause()
        model.saveToFiles(this)
    }

    override fun onResume() {
        super.onResume()
        model.readFromFiles(this)
    }
}

