package com.procyk.maciej.notes.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.procyk.maciej.notes.model.Note
import com.procyk.maciej.notes.model.NoteCollection
import java.io.BufferedInputStream
import java.io.FileNotFoundException

class NoteViewModel : ViewModel() {

    private var noteIndex: Int = 1

    private val _noteTitle = MutableLiveData<String>().apply {
        this.value = ""
    }

    val noteTitle: LiveData<String> = _noteTitle

    private val _noteDescription = MutableLiveData<String>().apply {
        this.value = ""
    }

    val noteDescription: LiveData<String> = _noteDescription

    private val _noteBody = MutableLiveData<String>().apply {
        this.value = ""
    }

    val noteBody: LiveData<String> = _noteBody

    private val notes: NoteCollection = NoteCollection().apply {
        repeat(3) {
        add(Note())
    } }

    private fun loadNote(note: Note) {
        _noteTitle.value = note.title
        _noteDescription.value = note.description
        _noteBody.value = note.body
    }

    private fun manageInvariant() {
        if (noteIndex == notes.size - 1) {
            notes.add(Note())
        }

        if (noteIndex == 0) {
            notes.addFirst(Note())
            noteIndex++
        }
    }

    fun saveAndLoadNextNote(title: String, description: String, body: String) {
        val tempNote = Note(title, description, body)

        manageInvariant()

        notes.removeAt(noteIndex)

        if (tempNote.isEmpty()) {
            loadNote(notes[noteIndex])
        }
        else {
            notes.add(noteIndex, tempNote)
            loadNote(notes[++noteIndex])
        }
    }

    fun saveAndLoadPrevNote(title: String, description: String, body: String) {
        val tempNote = Note(title, description, body)

        manageInvariant()

        if (tempNote.isEmpty()) {
            notes.removeAt(noteIndex--)
            loadNote(notes[noteIndex])
        }
        else {
            notes.removeAt(noteIndex)
            notes.add(noteIndex, tempNote)
            loadNote(notes[--noteIndex])
        }
    }

    fun saveToFiles(context: Context) {
        context.fileList().forEach { context.deleteFile(it) }
        notes.forEachIndexed { index, note ->
            val stream = context.openFileOutput(index.toString(), Context.MODE_PRIVATE)
            stream.use {
                note.writeToFile(it.bufferedWriter())
            }
        }
    }

    fun readFromFiles(context: Context) {
        notes.clear()
        var index = 0
        try {
            while (true) {
                val stream = context.openFileInput(index.toString())
                stream.use {
                    val lines = it.bufferedReader().readLines()
                    if (lines.size == 3) {
                        notes.add(Note(lines[0], lines[1], lines[2]))
                    }
                }
                index += 1
            }
        } catch (e: FileNotFoundException) {
            // opened all saved files
            if (index == 0) {
                notes.add(Note())
            }
        } finally {
            notes.addFirst(Note())
            notes.add(Note())
            noteIndex = 1;
            loadNote(notes[noteIndex])
        }
    }
}
