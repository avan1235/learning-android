package com.procyk.maciej.widgets.view

import android.content.Context
import android.graphics.*
import android.os.Bundle
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import com.procyk.maciej.widgets.R
import com.procyk.maciej.widgets.utils.name
import com.procyk.maciej.widgets.utils.toast
import kotlinx.android.synthetic.main.activity_widget.*

class WidgetsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_widget)

        nextButton.setOnClickListener { nextViews() }
        prevButton.setOnClickListener { prevViews() }
        manageViews()
    }

    companion object {
        const val COUNT = 20
    }

    private val buttons by lazy {
        val views = List(COUNT) { Button(this) }
        views.forEachIndexed { index, button ->
            button.addName(index + 1)
            button.setOnClickListener { toast("${button.text} clicked") }
        }
        views
    }

    private val radios by lazy {
        val views = List(COUNT) { RadioButton(this) }
        views.forEachIndexed { index, button ->
            button.addName(index + 1)
            if (index == 0) button.isChecked = true
            button.setOnClickListener {
                val radioButton = it as RadioButton
                views.forEachIndexed { idx, btn -> if (idx != index) btn.isChecked = false  }
                toast("${radioButton.text} checked")
            }
        }
        views
    }

    private val checkboxes by lazy { createCompoundsOf { CheckBox(it) } }

    private val textViews by lazy {
        val views = List(COUNT) { EditText(this) }
        views.forEachIndexed { index, textView ->
            textView.addName(index + 1, true)
            textView.addTextChangedListener(afterTextChanged = { text ->
                toast("${textView.hint}: $text")
            })
        }
        views
    }

    private val imageViews by lazy {
        val views = List(COUNT) { ImageView(this) }
        views.forEachIndexed { index, imageView ->
            imageView.setImageResource(R.drawable.ic_arrow_black)
            imageView.scaleType = ImageView.ScaleType.FIT_XY
            imageView.adjustViewBounds = true
            imageView.rotation = (index.toFloat() / COUNT * 2) * 360.0f
            imageView.setOnClickListener {
                toast("${it.name()} ${index + 1} clicked")
            }
        }
        views
    }

    private val switches by lazy { createCompoundsOf { Switch(it) } }

    private val seekBars by lazy {
        val views = List(COUNT) { SeekBar(this) }
        views.forEachIndexed { index, bar ->
            bar.setProgress(index * 100 / COUNT)
            bar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    this@WidgetsActivity.toast(String.format("%s: %d", seekBar?.getName(index + 1) ?: "", progress))
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit
                override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
            })
        }
        views
    }

    private val tableLayout by lazy {
        val layout = TableLayout(this)
        val params = TableLayout.LayoutParams(0, TableLayout.LayoutParams.WRAP_CONTENT)
        repeat(COUNT) {
            val row = TableRow(this)
            repeat(COUNT) { row.addView(randomColoredImageView()) }
            layout.addView(row, params)
        }
        layout.isStretchAllColumns = true
        listOf(layout)
    }

    enum class ViewType { BUTTON, RADIO, CHECKBOX, TEXT, IMAGE, SWITCH, SEEKBAR, TABLE }

    private var viewsIndex = 0

    private fun nextViews() {
        viewsIndex += 1
        viewsIndex %= ViewType.values().size
        manageViews()
    }

    private fun prevViews() {
        viewsIndex -= 1
        viewsIndex += ViewType.values().size
        viewsIndex %= ViewType.values().size
        manageViews()
    }

    private fun manageViews() {
        widgetsLayout.removeAllViews()
        when(viewsIndex) {
            ViewType.BUTTON.ordinal -> buttons
            ViewType.RADIO.ordinal -> radios
            ViewType.CHECKBOX.ordinal -> checkboxes
            ViewType.TEXT.ordinal -> textViews
            ViewType.IMAGE.ordinal -> imageViews
            ViewType.SWITCH.ordinal -> switches
            ViewType.SEEKBAR.ordinal -> seekBars
            ViewType.TABLE.ordinal -> tableLayout
            else -> listOf()
        }.apply {
            this.forEach { widgetsLayout.addView(it) }
            this.firstOrNull()?.let { titleView.text = it.name() }
        }
    }

    private fun TextView.addName(index: Int, hint: Boolean = false) {
        if (hint) {
            this.hint = getName(index)
        }
        else {
            this.text = getName(index)
        }
    }

    private fun Any.getName(index: Int) = String.format("%s %d", this.name(), index)

    private fun List<CompoundButton>.addCheckedListener() = this.forEachIndexed { index, button ->
        button.addName(index + 1)
        button.setOnCheckedChangeListener { _, isChecked ->
            toast("${button.text} ${if (!isChecked) "un" else ""}checked")
        }
    }

    private fun <T : CompoundButton> createCompoundsOf(creator: (Context) -> T): List<T> {
        val views = List(COUNT) { creator(this) }
        views.addCheckedListener()
        return views
    }

    private fun randomColoredImageView(): ImageView {
        val image = ImageView(this)
        val rnd = { (0..255).random() }
        image.setImageResource(R.drawable.block)
        image.setColorFilter(Color.argb(255, rnd(), rnd(), rnd()))
        image.scaleType = ImageView.ScaleType.FIT_XY
        image.adjustViewBounds = true
        return image
    }
}

