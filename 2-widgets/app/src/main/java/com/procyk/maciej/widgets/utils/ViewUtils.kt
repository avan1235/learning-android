package com.procyk.maciej.widgets.utils

import android.app.Activity
import android.widget.EditText
import android.widget.Toast

fun EditText?.extractText() = this?.text?.toString() ?: ""

fun Activity.toast(text: String) = Toast.makeText(this, text, Toast.LENGTH_SHORT).show()

fun Any.name(): String = this::class.java.simpleName
