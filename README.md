# Learing Android

This repository contains some base project from Android programming classes written in Kotlin language.
Additioanlly base CI setup was done for this project so every compiled (but not signed) application can be downloaded.

## [Notes](./1-notes)
[![](https://img.shields.io/gitlab/pipeline/avan1235/learning-android?label=notes-build)](https://gitlab.com/avan1235/learning-android/-/jobs/artifacts/master/raw/export/notes.apk?job=notes%20build)

Notes app for writing and saving notes on filesystem of the device

## [Widgets](./2-widgets)
[![](https://img.shields.io/gitlab/pipeline/avan1235/learning-android?label=widgets-build)](https://gitlab.com/avan1235/learning-android/-/jobs/artifacts/master/raw/export/widgets.apk?job=widgets%20build)

Widgets app for testing most of the basic widgets available in Android apps.

## [Threads](./3-threads)
[![](https://img.shields.io/gitlab/pipeline/avan1235/learning-android?label=threads-build)](https://gitlab.com/avan1235/learning-android/-/jobs/artifacts/master/raw/export/widgets.apk?job=simulation%20build)

Covid simulation app testing concurrency with Kotlin coroutines.